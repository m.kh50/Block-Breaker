﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	private Paddle paddle;
	private Vector3 PaddleToBall;
	private bool hasStarted = true;

	// Use this for initialization
	void Start () {
		paddle = FindObjectOfType<Paddle> ();
		PaddleToBall = this.transform.position - paddle.transform.position;

	}
	
	// Update is called once per frame
	void Update () {
		if (hasStarted == true) {
			this.transform.position = PaddleToBall + paddle.transform.position;
			if (Input.GetMouseButtonDown (0)) {
				this.rigidbody2D.velocity = new Vector2 (3f, 8f);
				hasStarted = false;
			}
		}
	}
}
