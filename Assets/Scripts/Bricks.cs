﻿using UnityEngine;
using System.Collections;

public class Bricks : MonoBehaviour {
	public int maxHits;
	private int timesHit;
	public LevelManager levelManager;
	public Sprite[] hitSprites;
	public static int breakableCount = 0;
	private bool isBreaakable;

	// Use this for initialization
	void Start () {
		isBreaakable = (this.tag == "Breakable");
		if(isBreaakable){
			breakableCount++;
		}
		timesHit = 0;
		levelManager = FindObjectOfType<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D(Collision2D collision){
		timesHit++;
		if (timesHit == maxHits) {
			breakableCount--;
			levelManager.BrickDestroyed();
			Destroy (gameObject);
		} else { LoadNextSprite();
		}

	}

	void LoadNextSprite(){
		int spriteIndex = timesHit - 1;
		this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
	}

	void SimulateWin(){
		levelManager.LoadNextLevel ();
	}
}
