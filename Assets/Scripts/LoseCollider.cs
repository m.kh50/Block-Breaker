﻿using UnityEngine;
using System.Collections;

public class LoseCollider : MonoBehaviour {
	private LevelManager levelManager;
	private int life = 100;

	void Start(){
		levelManager = FindObjectOfType<LevelManager>();
	}

	void OnTriggerEnter2D(Collider2D trigger){
		life--;
		if(life>=0){
			levelManager.LoadedLevel();
		} else{
			levelManager.LoadLevel("Lose");
		}
	}

}
